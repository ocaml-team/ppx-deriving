type t =
  | Leaf of int
  | Node of t list
  [@@deriving show]

let my_tree = (Node [(Leaf 17); (Leaf 42); (Leaf 73)]);;
            
Format.printf "tree: %a@." pp my_tree

