ppx-deriving (6.0.3-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.7.0

 -- Stéphane Glondu <glondu@debian.org>  Fri, 11 Oct 2024 11:54:40 +0200

ppx-deriving (6.0.2-2) unstable; urgency=medium

  * Update test for new show returned value (Closes: #1072672)

 -- Stéphane Glondu <glondu@debian.org>  Fri, 07 Jun 2024 09:13:54 +0200

ppx-deriving (6.0.2-1) unstable; urgency=medium

  * New upstream release

 -- Stéphane Glondu <glondu@debian.org>  Mon, 03 Jun 2024 14:45:24 +0200

ppx-deriving (5.2.1-4) unstable; urgency=medium

  * Use ocaml_dune DH buildsystem

 -- Stéphane Glondu <glondu@debian.org>  Sun, 06 Aug 2023 08:21:28 +0200

ppx-deriving (5.2.1-3) unstable; urgency=medium

  * Team upload.
  * Fix compilation with recent dune.

 -- Julien Puydt <jpuydt@debian.org>  Thu, 06 Jul 2023 11:09:39 +0200

ppx-deriving (5.2.1-2) unstable; urgency=medium

  * Team upload.
  * Fix d/watch.
  * Bump standards-version to 4.6.2.

 -- Julien Puydt <jpuydt@debian.org>  Mon, 03 Jul 2023 16:37:46 +0200

ppx-deriving (5.2.1-1) unstable; urgency=medium

  * New upstream release

 -- Stéphane Glondu <glondu@debian.org>  Fri, 21 Jan 2022 06:02:55 +0100

ppx-deriving (5.1-2) unstable; urgency=medium

  * Re-apply patch to allow unsafe modules

 -- Stéphane Glondu <glondu@debian.org>  Wed, 24 Nov 2021 11:38:09 +0100

ppx-deriving (5.1-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.6.0
  * Update debian/watch

 -- Stéphane Glondu <glondu@debian.org>  Fri, 19 Nov 2021 13:28:30 +0100

ppx-deriving (4.5-1) unstable; urgency=medium

  * New upstream release
  * Bump debhelper compat level to 13
  * Bump Standards-Version to 4.5.0

 -- Stéphane Glondu <glondu@debian.org>  Sat, 25 Jul 2020 14:18:55 +0200

ppx-deriving (4.4-2) unstable; urgency=medium

  * Call "Dynlink.allow_unsafe_modules true" in ppx_deriving_main

 -- Stéphane Glondu <glondu@debian.org>  Tue, 17 Sep 2019 14:07:35 +0200

ppx-deriving (4.4-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.4.0

 -- Stéphane Glondu <glondu@debian.org>  Mon, 09 Sep 2019 10:14:45 +0200

ppx-deriving (4.2.1-3) unstable; urgency=medium

  * Standards-version 4.3.0 (no change)
  * Added myself to Uploaders
  * Debhelper compatibility level 12:
    - override dh_dwz to use --no-dwz-multifile

 -- Ralf Treinen <treinen@debian.org>  Mon, 07 Jan 2019 19:45:28 +0100

ppx-deriving (4.2.1-2) unstable; urgency=medium

  * Team upload
  * Update Vcs-* to salsa
  * Standards-Version 4.1.5 (no change)

 -- Ralf Treinen <treinen@debian.org>  Mon, 09 Jul 2018 22:25:46 +0200

ppx-deriving (4.2.1-1) unstable; urgency=medium

  * Team upload
  * New upstream version.
    - Dropped patch 0002-Fix-typo-in-ppx_deriving_make.mllib.patch which has
      been applied upstream.
    - Dropped patch b6b02c2ba051bde67c2b454724ff0bf36bc11060.patch since
      issue is resolved upstream.
  * Add minimal versions to build-dependencies ocaml-nox, ocaml-findlib,
    libppx-tools-ocaml-dev according to upstream's opam file.
  * Add build-dependencies:
    - libppx-derivers-ocaml-dev,
    - libmigrate-parsetree-ocaml-dev
  * Standards-Version 4.1.3:
    - d/copyright: use https in format specifier.
  * Add dep8-style package test
  * d/rules: use dh_missing (instead of option to dh_install)
  * debhelper compatibility level 11

 -- Ralf Treinen <treinen@debian.org>  Tue, 20 Mar 2018 21:21:23 +0100

ppx-deriving (4.1-1.1) unstable; urgency=medium

  * Non-maintainer upload
  * debian/patches/b6b02c2ba051bde67c2b454724ff0bf36bc11060.patch:
    - upstream fix for ocaml build failure (Closes: #876725)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sat, 28 Oct 2017 13:37:32 +0200

ppx-deriving (4.1-1) unstable; urgency=medium

  * New upstream release
  * Add ocamlbuild to Build-Depends

 -- Stéphane Glondu <glondu@debian.org>  Fri, 21 Jul 2017 17:37:11 +0200

ppx-deriving (4.0-1) unstable; urgency=medium

  * New upstream release

 -- Stéphane Glondu <glondu@debian.org>  Fri, 05 Aug 2016 08:57:01 +0200

ppx-deriving (3.3-2) unstable; urgency=medium

  * Fix FTBFS on bytecode architectures
  * Add versioned dependency for ppx-tools
  * Fix debian/watch

 -- Stéphane Glondu <glondu@debian.org>  Thu, 04 Aug 2016 10:14:48 +0200

ppx-deriving (3.3-1) unstable; urgency=low

  * Initial release (Closes: #832881)

 -- Stéphane Glondu <glondu@debian.org>  Fri, 29 Jul 2016 12:54:49 +0200
